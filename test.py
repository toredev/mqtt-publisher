
import datetime
from random import randint
import json

sampleMessage1 = [
    {
        "signal": "equipment_serial",
        "timestamp": "2018-08-T06:57:02.000Z",
        "value_str": "K14845"
    },
    {
        "signal": "equipment_name",
        "timestamp": "2018-08-T06:57:02.000Z",
        "value_str": "Karkaisimo"
    },
    {
        "signal": "equipment_type",
        "timestamp": "2018-08-T06:57:02.000Z",
        "value_str": "CXT Auto"
    },
    {
        "signal": "plc_sw_version",
        "timestamp": "2018-08-T06:57:02.000Z",
        "value_str": "1.2.3"
    },
    {
        "signal": "diag_trigger_0001",
        "timestamp": "2018-08-T06:57:02.000Z",
        "value_bool": 0
    },
    {
        "signal": "diag_trigger_0002",
        "timestamp": "2018-08-T06:57:02.000Z",
        "value_bool": 0
    },
    {
        "signal": "diag_trigger_0003",
        "timestamp": "2018-08-T06:57:02.000Z",
        "value_bool": 0
    },
    {
        "signal": "diag_trigger_0004",
        "timestamp": "2018-08-T06:57:02.000Z",
        "value_bool": 0
    }
]

sampleMessage2 = [
    {
        "signal": "equipment_serial",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_str": "26659657-2"
    },
    {
        "signal": "equipment_name",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_str": "Demag Training Crane"
    },
    {
        "signal": "equipment_type",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_str": "DMR"
    },
    {
        "signal": "plc_sw_version",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_str": "5.11"
    },
    {
        "signal": "diag_code_0001",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_dbl": 1
    },
    {
        "signal": "diag_code_0002",
        "timestamp": "2018-08-08T06:57:02.000Z",
        "value_dbl": 2
    },
    {
        "signal": "diag_code_0003",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 92
    },
    {
        "signal": "diag_code_0004",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0005",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0006",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0007",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0008",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0009",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0010",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0011",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0012",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0013",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0014",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0015",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0016",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    }
]

sampleMessage3 = [
    {
        "signal": "equipment_serial",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_str": "26659657-2"
    },
    {
        "signal": "equipment_name",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_str": "Demag Training Crane"
    },
    {
        "signal": "equipment_type",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_str": "DMR"
    },
    {
        "signal": "plc_sw_version",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_str": "5.11"
    },
    {
        "signal": "diag_code_0001",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_dbl": 1
    },
    {
        "signal": "diag_code_0002",
        "timestamp": "2018-08-08T06:57:02.000Z",
        "value_dbl": 2
    },
    {
        "signal": "diag_code_0003",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 92
    },
    {
        "signal": "diag_code_0004",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0005",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0006",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0007",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0008",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0009",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0010",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0011",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0012",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0013",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0014",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0015",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0016",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 506 - Hour counter (1)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 507 - Hour counter (2)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 508 - Hour counter (3)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 509 - Hour counter (4)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 510 - Hour counter (5)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 511 - Hour counter (6)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 512 - Hour counter (7)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 513 - Hour counter (8)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 514 - Hour counter (9)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 515 - Hour counter (10)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 516 - Hour counter (11)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 517 - Hour counter (12)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 518 - Hour counter (13)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 519 - Hour counter (14)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 520 - Hour counter (15)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 521 - Hour counter (16)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    }
]


def generate_message1():
    timestamp = str(datetime.datetime.now())
    rand = randint(1, 4)
    generated_data = []
    for item in sampleMessage1:
        item["timestamp"] = timestamp
        if "diag_trigger_000" + str(rand) in item["signal"]:
            item["value_str"] = "1"
        generated_data.append(item)
    return generated_data


def generate_message2():
    generated_data = []
    for item in sampleMessage2:
        new_item = {}
        timestamp = str(datetime.datetime.now())
        rand = randint(100, 9999)/100
        new_item["signal"] = item["signal"]
        new_item["timestamp"] = timestamp
        if "value_dbl" in item:
            new_item["value_dbl"] = rand
        else:
            new_item["value_str"] = item["value_str"]
        generated_data.append(new_item)
    return generated_data


def generate_message3():
    generated_data = []
    for item in sampleMessage3:
        new_item = {}
        timestamp = str(datetime.datetime.now())
        rand = randint(100, 9999) / 100
        new_item["signal"] = item["signal"]
        new_item["timestamp"] = timestamp
        if "value_dbl" in item:
            new_item["value_dbl"] = rand
        else:
            new_item["value_str"] = item["value_str"]
        generated_data.append(new_item)
    return generated_data


def generate_dummy_data():
    rand = randint(1, 3)
    if rand == 1:
        return generate_message1()
    if rand == 2:
        return generate_message2()
    if rand == 3:
        return generate_message3()


print(json.dumps(generate_message3(), indent=4))
