# /*
# * Copyright 2010-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
# *
# * Licensed under the Apache License, Version 2.0 (the "License").
# * You may not use this file except in compliance with the License.
# * A copy of the License is located at
# *
# *  http://aws.amazon.com/apache2.0
# *
# * or in the "license" file accompanying this file. This file is distributed
# * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
# * express or implied. See the License for the specific language governing
# * permissions and limitations under the License.
# */


import os
import sys
import time
import uuid
import json
import logging
import argparse
import datetime
from random import randint
from AWSIoTPythonSDK.core.greengrass.discovery.providers import DiscoveryInfoProvider
from AWSIoTPythonSDK.core.protocol.connection.cores import ProgressiveBackOffCore
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
from AWSIoTPythonSDK.exception.AWSIoTExceptions import DiscoveryInvalidRequestException

AllowedActions = ['both', 'publish', 'subscribe']

# General message notification callback
def customOnMessage(message):
    print('Received message on topic %s: %s\n' % (message.topic, message.payload))

MAX_DISCOVERY_RETRIES = 10
GROUP_CA_PATH = "./groupCA/"

# Read in command-line parameters
'''parser = argparse.ArgumentParser()
parser.add_argument("-e", "--endpoint", action="store", required=True, dest="host", help="Your AWS IoT custom endpoint")
parser.add_argument("-r", "--rootCA", action="store", required=True, dest="rootCAPath", help="Root CA file path")
parser.add_argument("-c", "--cert", action="store", dest="certificatePath", help="Certificate file path")
parser.add_argument("-k", "--key", action="store", dest="privateKeyPath", help="Private key file path")
parser.add_argument("-n", "--thingName", action="store", dest="thingName", default="Bot", help="Targeted thing name")
parser.add_argument("-t", "--topic", action="store", dest="topic", default="sdk/test/Python", help="Targeted topic")
parser.add_argument("-m", "--mode", action="store", dest="mode", default="both",
                    help="Operation modes: %s"%str(AllowedActions))
parser.add_argument("-M", "--message", action="store", dest="message", default="Hello World!",
                    help="Message to publish")

args = parser.parse_args()'''

#Hard coded - really bad. It shouldnt be like that
host = "a2e5bzlwjnprrv.iot.eu-central-1.amazonaws.com"
rootCAPath = "/gg-publisher/certs/root-ca.pem"
certificatePath = "/gg-publisher/certs/cert.pem"
privateKeyPath = "/gg-publisher/certs/privkey.pem"
clientId = "local_publisher"
thingName = "local_publisher"
topic = "invoke/local"
mode = "publish"

if mode not in AllowedActions:
    parser.error("Unknown --mode option %s. Must be one of %s" % (mode, str(AllowedActions)))
    exit(2)

# Configure logging
logger = logging.getLogger("AWSIoTPythonSDK.core")
logger.setLevel(logging.DEBUG)
streamHandler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

# Progressive back off core
backOffCore = ProgressiveBackOffCore()

# Discover GGCs
discoveryInfoProvider = DiscoveryInfoProvider()
discoveryInfoProvider.configureEndpoint(host)
discoveryInfoProvider.configureCredentials(rootCAPath, certificatePath, privateKeyPath)
discoveryInfoProvider.configureTimeout(10)  # 10 sec

retryCount = MAX_DISCOVERY_RETRIES
discovered = False
groupCA = None
coreInfo = None
while retryCount != 0:
    try:
        discoveryInfo = discoveryInfoProvider.discover(thingName)
        caList = discoveryInfo.getAllCas()
        coreList = discoveryInfo.getAllCores()

        # We only pick the first ca and core info
        groupId, ca = caList[0]
        coreInfo = coreList[0]
        print("Discovered GGC: %s from Group: %s" % (coreInfo.coreThingArn, groupId))

        print("Now we persist the connectivity/identity information...")
        groupCA = GROUP_CA_PATH + groupId + "_CA_" + str(uuid.uuid4()) + ".crt"
        if not os.path.exists(GROUP_CA_PATH):
            os.makedirs(GROUP_CA_PATH)
        groupCAFile = open(groupCA, "w")
        groupCAFile.write(ca)
        groupCAFile.close()

        discovered = True
        print("Now proceed to the connecting flow...")
        break
    except DiscoveryInvalidRequestException as e:
        print("Invalid discovery request detected!")
        print("Type: %s" % str(type(e)))
        print("Error message: %s" % e.message)
        print("Stopping...")
        break
    except BaseException as e:
        print("Error in discovery!")
        print("Type: %s" % str(type(e)))
        print("Error message: %s" % e.message)
        retryCount -= 1
        print("\n%d/%d retries left\n" % (retryCount, MAX_DISCOVERY_RETRIES))
        print("Backing off...\n")
        backOffCore.backOff()

if not discovered:
    print("Discovery failed after %d retries. Exiting...\n" % (MAX_DISCOVERY_RETRIES))
    sys.exit(-1)

# Iterate through all connection options for the core and use the first successful one
myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId)
myAWSIoTMQTTClient.configureCredentials(groupCA, privateKeyPath, certificatePath)
myAWSIoTMQTTClient.onMessage = customOnMessage

connected = False
for connectivityInfo in coreInfo.connectivityInfoList:
    currentHost = connectivityInfo.host
    currentPort = connectivityInfo.port
    print("Trying to connect to core at %s:%d" % (currentHost, currentPort))
    myAWSIoTMQTTClient.configureEndpoint(currentHost, currentPort)
    try:
        myAWSIoTMQTTClient.connect()
        connected = True
        break
    except BaseException as e:
        print("Error in connect!")
        print("Type: %s" % str(type(e)))
        print("Error message: %s" % e.message)

if not connected:
    print("Cannot connect to core %s. Exiting..." % coreInfo.coreThingArn)
    sys.exit(-2)

# Successfully connected to the core
if mode == 'both' or mode == 'subscribe':
    myAWSIoTMQTTClient.subscribe(topic, 0, None)
time.sleep(2)

sampleMessage1 = [
    {
        "signal": "equipment_serial",
        "timestamp": "2018-08-T06:57:02.000Z",
        "value_str": "K14845"
    },
    {
        "signal": "equipment_name",
        "timestamp": "2018-08-T06:57:02.000Z",
        "value_str": "Karkaisimo"
    },
    {
        "signal": "equipment_type",
        "timestamp": "2018-08-T06:57:02.000Z",
        "value_str": "CXT Auto"
    },
    {
        "signal": "plc_sw_version",
        "timestamp": "2018-08-T06:57:02.000Z",
        "value_str": "1.2.3"
    },
    {
        "signal": "diag_trigger_0001",
        "timestamp": "2018-08-T06:57:02.000Z",
        "value_bool": 0
    },
    {
        "signal": "diag_trigger_0002",
        "timestamp": "2018-08-T06:57:02.000Z",
        "value_bool": 0
    },
    {
        "signal": "diag_trigger_0003",
        "timestamp": "2018-08-T06:57:02.000Z",
        "value_bool": 0
    },
    {
        "signal": "diag_trigger_0004",
        "timestamp": "2018-08-T06:57:02.000Z",
        "value_bool": 0
    }
]

sampleMessage2 = [
    {
        "signal": "equipment_serial",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_str": "26659657-2"
    },
    {
        "signal": "equipment_name",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_str": "Demag Training Crane"
    },
    {
        "signal": "equipment_type",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_str": "DMR"
    },
    {
        "signal": "plc_sw_version",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_str": "5.11"
    },
    {
        "signal": "diag_code_0001",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_dbl": 1
    },
    {
        "signal": "diag_code_0002",
        "timestamp": "2018-08-08T06:57:02.000Z",
        "value_dbl": 2
    },
    {
        "signal": "diag_code_0003",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 92
    },
    {
        "signal": "diag_code_0004",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0005",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0006",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0007",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0008",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0009",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0010",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0011",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0012",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0013",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0014",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0015",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0016",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    }
]

sampleMessage3 = [
    {
        "signal": "equipment_serial",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_str": "26659657-2"
    },
    {
        "signal": "equipment_name",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_str": "Demag Training Crane"
    },
    {
        "signal": "equipment_type",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_str": "DMR"
    },
    {
        "signal": "plc_sw_version",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_str": "5.11"
    },
    {
        "signal": "diag_code_0001",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_dbl": 1
    },
    {
        "signal": "diag_code_0002",
        "timestamp": "2018-08-08T06:57:02.000Z",
        "value_dbl": 2
    },
    {
        "signal": "diag_code_0003",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 92
    },
    {
        "signal": "diag_code_0004",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0005",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0006",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0007",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0008",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0009",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0010",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0011",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0012",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0013",
        "timestamp": "2018-08-08T06:58:02.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0014",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0015",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "diag_code_0016",
        "timestamp": "2018-08-01T01:02:03.000Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 506 - Hour counter (1)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 507 - Hour counter (2)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 508 - Hour counter (3)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 509 - Hour counter (4)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 510 - Hour counter (5)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 511 - Hour counter (6)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 512 - Hour counter (7)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 513 - Hour counter (8)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 514 - Hour counter (9)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 515 - Hour counter (10)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 516 - Hour counter (11)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 517 - Hour counter (12)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 518 - Hour counter (13)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 519 - Hour counter (14)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 520 - Hour counter (15)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    },
    {
        "signal": "DR - 521 - Hour counter (16)",
        "timestamp": "2018-10-03T10:55:21.719Z",
        "value_dbl": 0
    }
]


def generate_message1():
    timestamp = str(datetime.datetime.now())
    rand = randint(1, 4)
    generated_data = []
    for item in sampleMessage1:
        item["timestamp"] = timestamp
        if "diag_trigger_000" + str(rand) in item["signal"]:
            item["value_str"] = "1"
        generated_data.append(item)
    return generated_data


def generate_message2():
    generated_data = []
    for item in sampleMessage2:
        new_item = {}
        timestamp = str(datetime.datetime.now())
        rand = randint(100, 9999)/100
        new_item["signal"] = item["signal"]
        new_item["timestamp"] = timestamp
        if "value_dbl" in item:
            new_item["value_dbl"] = rand
        else:
            new_item["value_str"] = item["value_str"]
        generated_data.append(new_item)
    return generated_data


def generate_message3():
    generated_data = []
    for item in sampleMessage3:
        new_item = {}
        timestamp = str(datetime.datetime.now())
        rand = randint(100, 9999) / 100
        new_item["signal"] = item["signal"]
        new_item["timestamp"] = timestamp
        if "value_dbl" in item:
            new_item["value_dbl"] = rand
        else:
            new_item["value_str"] = item["value_str"]
        generated_data.append(new_item)
    return generated_data


def generate_dummy_data():
    rand = randint(1, 3)
    if rand == 1:
        return generate_message1()
    if rand == 2:
        return generate_message2()
    if rand == 3:
        return generate_message3()


loopCount = 0
while loopCount < 5:
    if mode == 'both' or mode == 'publish':      
        message = {}
        message['message'] = generate_dummy_data()
        message['sequence'] = loopCount
        messageJson = json.dumps(message)
        myAWSIoTMQTTClient.publish(topic, messageJson, 0)
        if mode == 'publish':
            print('Published topic %s: %s\n' % (topic, messageJson))
        loopCount += 1
    time.sleep(3)
